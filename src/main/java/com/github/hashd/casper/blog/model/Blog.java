package com.github.hashd.casper.blog.model;

import com.github.hashd.casper.security.model.User;

import java.util.Date;
import java.util.Set;

/**
 * Created by kd on 27/5/15.
 */
public class Blog {
    private User author;
    private Date createdDate;
    private Date modifiedDate;
    private Set<Comment> comments;

}

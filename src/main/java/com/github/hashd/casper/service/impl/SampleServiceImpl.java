package com.github.hashd.casper.service.impl;

import com.github.hashd.casper.service.SampleService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Created by kd on 24/5/15.
 */
@Service("sampleService")
public class SampleServiceImpl implements SampleService {

    @Value("${author.name:World}")
    private String name;

    public String welcome() {
        return "Hello, " + name;
    }
}

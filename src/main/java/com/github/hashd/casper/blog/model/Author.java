package com.github.hashd.casper.blog.model;

import org.springframework.data.annotation.Id;

/**
 * Created by kd on 27/5/15.
 */
public class Author {
    @Id
    private String id;
    private String emailAddress;
    private String name;
    private String username;
    private String mobileNumber;
    private char   gender;

    public Author() {
    }

    public Author(String username, String emailAddress, String name, char gender, String mobileNumber) {
        this.emailAddress = emailAddress;
        this.name = name;
        this.username = username;
        this.gender = gender;
        this.mobileNumber = mobileNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
}

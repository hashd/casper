package com.github.hashd.casper.web;

import com.github.hashd.casper.config.meta.Author;
import com.github.hashd.casper.service.SampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * Created by kd on 24/5/15.
 */
@Controller
public class GhostController {
    @Autowired
    SampleService sampleService;

    @Autowired
    Author author;

    @ResponseBody
    @RequestMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public String home() {
        return sampleService.welcome();
    }

    @ResponseBody
    @RequestMapping(value = "/author/information", produces = MediaType.APPLICATION_JSON_VALUE)
    public Author authorInfo() {
        return author;
    }

    @RequestMapping(value = "/welcome/{name}", method = RequestMethod.GET)
    public String welcome(@PathVariable(value = "name") String name, @RequestParam(value = "greeting", defaultValue = "Hello") String greeting, Model model) {
        model.addAttribute("greeting", greeting);
        model.addAttribute("name", name);
        return "welcome";
    }
}

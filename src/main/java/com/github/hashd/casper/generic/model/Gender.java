package com.github.hashd.casper.generic.model;

/**
 * Created by kd on 27/5/15.
 */
public enum Gender {
    MALE('M', "Male"), FEMALE('F', "Female");

    private char code;
    private String description;

    Gender(char code, String description) {
        this.code = code;
        this.description = description;
    }
}

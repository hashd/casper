package com.github.hashd.casper.security.model;

import com.github.hashd.casper.generic.model.Gender;

/**
 * Created by kd on 24/5/15.
 */
//@Entity(name = "users")
public class User {
    public static User anonymous = new User();

    private int          userId;
    private UserSecurity userSecurity;
    private String       firstName;
    private String       lastName;
    private String       middleName;
    private String       emailAddress;
    private Gender       gender;

}

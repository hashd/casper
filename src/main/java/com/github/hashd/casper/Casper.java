package com.github.hashd.casper;

import com.github.hashd.casper.blog.dao.AuthorRepository;
import com.github.hashd.casper.blog.model.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

/**
 * Created by kd on 24/5/15.
 */
@SpringBootApplication
@ImportResource(value = "classpath:config/casper-context.xml")
public class Casper implements CommandLineRunner {

    @Autowired
    private AuthorRepository authorRepository;

    public static void main(String[] args) {
        SpringApplication.run(Casper.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {
        authorRepository.deleteAll();

        authorRepository.save(new Author("clark.k", "clark.kent@dccomics.com", "Clark Kent", 'M', "9876543210"));
        authorRepository.save(new Author("widow.b", "black.widow@marvel.com", "Black Widow", 'F', "9751302468"));
        authorRepository.save(new Author("witch.s", "scarlet.witch@marvel.com", "Scarlet Witch", 'F', "9856321470"));
        authorRepository.save(new Author("silver.q", "quicksilver@marvel.com", "Quicksilver", 'M', "9874102365"));
    }
}

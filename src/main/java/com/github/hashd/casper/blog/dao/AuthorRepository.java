package com.github.hashd.casper.blog.dao;

import com.github.hashd.casper.blog.model.Author;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by kd on 29/5/15.
 */
public interface AuthorRepository extends MongoRepository<Author, String> {
    Author findByName(String name);
    List<Author> findByGender(char gender);
}

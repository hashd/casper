package com.github.hashd.casper.web;

import com.github.hashd.casper.blog.dao.AuthorRepository;
import com.github.hashd.casper.blog.model.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by kd on 29/5/15.
 */
@RestController
@RequestMapping("/author")
public class AuthorController {
    @Autowired
    private AuthorRepository authorRepository;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public List<Author> getAuthors() {
        return authorRepository.findByGender('F');
        //return authorRepository.findAll();
    }
}
